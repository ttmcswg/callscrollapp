import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ListView,
  ActivityIndicator,
   TouchableHighlight
} from 'react-native';
import { Constants } from 'expo';
import call from 'react-native-phone-call'

export default class Application extends Component {
  constructor(props) {
    super(props);
    this.fetchMore = this._fetchMore.bind(this);
    this.fetchData = this._fetchData.bind(this);

    this.reachedBottomCounter = 0;
    this.loadStep = 20;
    this.allRecordsList = [];
    this.itemsToDisplay = [];

    this.state = {
      dataSource: null,
      isLoading: true,
      isLoadingMore: false
    };
  }

  _onPressButton = (number) => {
    return () => {
        call({
            number:number,
            prompt:false
        }).catch(console.error)
    }
};

  _fetchData(callback) {

    fetch(`https://binariks.org/test.json`)
  
    //if you want data from server Json uncomment this peace of code
      .then(response => response.json())

      //If you want to mock json you can uncomment this peace of code 
      // .then(() => {
      //   arr = [];
      //   for(let i= 1; i< 100; i++) {
      //     arr.push({name: `petro ${i}`, phone: 123456789})
      //   }
      //   return arr})

      .then(callback)
      .catch(error => {
        console.error(error);
      });
  }

  _fetchMore() {

    var startFrom = this.reachedBottomCounter * this.loadStep;
    var endOn = startFrom + this.loadStep;
    
    if(endOn > this.allRecordsList.length) {
      endOn = endOn - (endOn - this.allRecordsList.length)
    }

    this.reachedBottomCounter++;

    for(let i = startFrom; i < endOn; i ++ ){
       this.itemsToDisplay.push(this.allRecordsList[i]);
    }
          
    this.setState({
        dataSource: this.state.dataSource.cloneWithRows(this.itemsToDisplay),
        isLoadingMore: false
      });

  }

  componentDidMount() {
    
    this.fetchData(responseJson => {

      let ds = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2,
      });

      this.allRecordsList = responseJson;
      const preRenderData = responseJson.slice(0,this.loadStep-1);

      this.setState({
        dataSource: ds.cloneWithRows(preRenderData),
        isLoading: false
      });

    });
    
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View >
          <ActivityIndicator size="large" />
        </View>
      );
    } else {
      return (
        <ListView
          dataSource={this.state.dataSource}
          renderRow={rowData => {
            return (
                <TouchableHighlight underlayColor="#ccc" onPress={this._onPressButton(rowData.phone) }>
              <View style={styles.listItem}>
                <View style={{ flex: 1 }}>
                  <Text style={styles.item}>
                    {rowData.name} 
                       -   
                    {rowData.phone}
                  </Text>
                </View>
              </View>
              </TouchableHighlight>
            );
          }}
          onEndReached={() =>
            this.setState({ isLoadingMore: true }, () => this.fetchMore())}
          renderFooter={() => {
            return (
              this.state.isLoadingMore &&
              <View style={{ flex: 1, padding: 10 }}>
                <ActivityIndicator size="small" />
              </View>
            );
          }}
        />
      );
    }
  }
}

const styles = StyleSheet.create({
  listItem: {
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#d6d7da',
    padding: 15
  },
 item: {
    fontSize: 20,
    textAlign: 'left',
    margin: 6
  }
});